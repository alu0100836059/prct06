require 'spec_helper'


#describe "practica6" do

#describe "~Bibliografia~" do
    
#it "Autor" do
  #      @prc = Bibliografia.new(["cerdo","as","as"],'tit','asd','asd','asd','asd','asd')
#@prc.autor.should == ["cerdo","as","as"]
    
#end
#end




#describe Pract6 do
  
  
  


  
  

describe "~Bibliografia~" do
    
it "Autor" do
@prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])       
@prc.autor.should == ['Dave Thomas', 'Andy Hunt','Chad Fowler']
    
end
    

it "Titulo" do
@prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])        
@prc.titulo.should =="Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide"
    
end
    

it "Serie" do
@prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])        
@prc.serie.should =="(The Facets of Ruby)"
    
end
    

it "Editorial" do
@prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])        
@prc.editorial.should =="Pragmatic Bookshelf"
    
end
    

it "Edicion" do
 @prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])       
@prc.edicion.should =="4 Edition"
    
end
    

it "Fecha" do
  @prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])      
@prc.fecha.should =="(July 7, 2013)"
    
end
    
it "ISBN" do
  @prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])      
@prc.ISBN.should ==["ISBN-13: 978-1937785499", "ISBN-10:1937785491"]
    
end
    
  
end

  


describe "Funciones adicionales" do
    
    
it "Funcion get_autor" do
   @prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])     
@prc.get_autor.should ==['Dave Thomas', 'Andy Hunt','Chad Fowler']
    
end
    

it "Funcion get_titulo" do
    @prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])    
@prc.get_titulo.should =="Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide"
    
end
    

it "Funcion get_serie" do
   @prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])     
@prc.get_serie.should =="(The Facets of Ruby)"
    
end
    

it "Funcion get_editorial" do
   @prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])     
@prc.get_editorial.should =="Pragmatic Bookshelf"
    
end
   

it "Funcion get_edicion" do
    @prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])    
@prc.get_edicion.should =="4 Edition"
    
end
    

it "Funcion get_fecha" do
      @prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])  
@prc.get_fecha.should =="(July 7, 2013)"
    
end
    

it "Funcion get_ISBN" do
      @prc = Bibliografia.new(["Dave Thomas","Andy Hunt", "Chad Fowler"],"Programming Ruby 1.9 & 2.0: The Pragmatic Programmers’ Guide","(The Facets of Ruby)","Pragmatic Bookshelf", "4 Edition", "(July 7, 2013)",["ISBN-13: 978-1937785499", "ISBN-10:1937785491"])  
@prc.get_ISBN.should ==["ISBN-13: 978-1937785499", "ISBN-10:1937785491"]
    
end
    
  
end
  
  

